;;;; package.lisp

(defpackage #:render-play
  (:use #:cl #:cepl #:rtg-math #:nineveh
        #:vari #:cepl.skitter.sdl2 :temporal-functions))
