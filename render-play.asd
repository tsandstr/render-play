;;;; render-play.asd

(asdf:defsystem #:render-play
  :description "Experiment with rendering in CEPL"
  :author "Theo Sandstrom <theo.j.sandstrom@gmail.com>"
  :license  "GPL v3.0"
  :version "0.0.1"
  :serial t
  :depends-on (#:cepl.sdl2 #:nineveh :dirt
                           :cepl.skitter.sdl2
                           :temporal-functions)
  :components ((:file "package")
               (:file "render-play")))
