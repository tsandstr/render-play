;;;; render-play.lisp

(in-package #:render-play)

(defvar *buf-stream* nil)
(defvar *gpu-arr* nil)

(defvar *cube-stream* nil)
(defvar *sphere-stream* nil)

(defvar *albedo-sampler* nil)
(defvar *specular-sampler* nil)

;; Camera stuff

(defclass camera ()
  ((pos :initform (v! 0 20 0) :accessor pos)
   (rot :initform (q:identity) :accessor rot)))

(defvar *camera* (make-instance 'camera))

(defun get-world->view-space (camera)
  (m4:* (m4:translation (v3:negate (pos camera)))
        (q:to-mat4 (q:inverse (rot camera)))))

(defun update-camera (camera)
  (when (keyboard-button (keyboard) key.w)
    (setf (pos camera)
          (v3:+ (pos camera)
                (v3:*s (q:to-direction (rot camera))
                       (* 10 *delta*)))))
  (when (keyboard-button (keyboard) key.s)
    (setf (pos camera)
          (v3:- (pos camera)
                (v3:*s (q:to-direction (rot camera))
                       (* 10 *delta*)))))
  (when (mouse-button (mouse) mouse.left)
    (let* ((move (mouse-move (mouse)))
           (move (v2:*s move 0.01)))
      (setf (rot camera)
            (q:normalize
             (q:* (rot camera)
                  (q:normalize (q:* (q:from-axis-angle (v! -1 0 0) (y move))
                                    (q:from-axis-angle (v! 0 -1 0) (x move))))))))))

;; Thing stuff

(defclass thing ()
  ((buf-stream :initarg :buf-stream :initform nil :accessor buf-stream)
   (pos :initarg :pos :initform (v! 0 0 0) :accessor pos)
   (rot :initarg :rot :initform (q:identity) :accessor rot)))

(defun get-model->world-space (thing)
  (m4:* (m4:translation (pos thing))
        (q:to-mat4 (rot thing))))

(defun draw-thing (thing cam)
  (map-g #'some-pipeline (buf-stream thing)
         :now (now)
         :model->world (get-model->world-space thing)
         :world->view (get-world->view-space cam)
         :view->clip (rtg-math.projection:perspective
                      (x (resolution (current-viewport)))
                      (y (resolution (current-viewport)))
                      0.1
                      200f0
                      60f0)
         :light-pos *light-pos*
         :albedo-samp *albedo-sampler*
         :specular-samp *specular-sampler*))

;; Light stuff

(defvar *light-pos* (v! 0 30 -5))

;; Main pipeline

(defun-g some-vert-stage ((vert g-pnt)
                          &uniform (now :float)
                          (model->world :mat4)
                          (world->view :mat4)
                          (view->clip :mat4))
  (let* ((pos (pos vert))
         (norm (norm vert))
         (uv (tex vert))

         (model->world-mat3 (m4:to-mat3 model->world))
         (world->view-mat3 (m4:to-mat3 world->view))
         (view->clip-mat3 (m4:to-mat3 view->clip))

         (pos (v! pos 1))

         (pos (* model->world pos))
         (norm (* model->world-mat3 norm))
         (pos (* world->view pos))
         (norm (* world->view-mat3 norm))
         (pos (* view->clip pos))
         (norm (* view->clip-mat3 norm)))
    (values pos
            (s~ pos :xyz)
            norm
            uv)))

(defun-g some-frag-stage ((pos :vec3)
                          (norm :vec3)
                          (uv :vec2)
                          &uniform (light-pos :vec3)
                          (albedo-samp :sampler-2d)
                          (specular-samp :sampler-2d))
  (let* ((object-color (texture albedo-samp uv))
         (specular-power (x (texture specular-samp uv)))

         (norm (normalize norm))

         (ambient 0.1)

         (cam-pos (v! 0 0 0))

         (dir-to-light (normalize (- light-pos pos)))
         (diffuse (saturate (dot dir-to-light norm)))

         (dir-to-cam (normalize (- cam-pos pos)))
         (dir-reflection (normalize (reflect (- dir-to-light) norm)))
         (specular (* (expt (saturate (dot dir-reflection dir-to-cam))
                            32f0) specular-power))

         (light-amount (+ ambient
                          diffuse
                          specular)))

    (* object-color light-amount)))

(defpipeline-g some-pipeline ()
  (some-vert-stage g-pnt)
  (some-frag-stage :vec3 :vec3 :vec2))

;; Main loop

(defun now ()
  (/ (float (get-internal-real-time))
     5000))

(defvar *fps* 0)
(defvar *fps-wip* 0)
(defvar *stepper* (make-stepper (seconds 1)))
(defvar *delta* 1)

(defvar *cubes* (loop :for i :below 40 :collect
                                       (make-instance
                                        'thing
                                        :pos (v3:+ (v! 0 0 -25)
                                                   (v! (- (random 20) 10)
                                                       (random 40)
                                                       (- (random 20) 10)))
                                        :rot (q:from-fixed-angles-v3
                                              (v! (- (random 20) 10)
                                                  (random 40)
                                                  (- (random 20) 10)))
                                        :buf-stream *cube-stream*)))

(defun draw ()
  (incf *fps-wip*)
  (when (funcall *stepper*)
    (setf *fps* *fps-wip*
          *fps-wip* 0))

  (setf *delta* (/ 1f0 *fps*))

  (step-host)

  (let ((val (* 10 (now))))
    (setf *light-pos* (v! (* 20 (sin val))
                          20
                          (- (* 20 (cos val)) 14))))

  (update-camera *camera*)

  (setf (resolution (current-viewport))
        (surface-resolution (current-surface (cepl-context))))

  (clear)

  (loop :for cube :in *cubes* :do
        (draw-thing cube *camera*))

  (swap)

  (decay-events))

(defun init ()
  (unless *buf-stream*
    (destructuring-bind (vert index)
        (nineveh.mesh.data.primitives:sphere-gpu-arrays)
      (setf *buf-stream*
            (make-buffer-stream vert :index-array index))))

  (unless *sphere-stream*
    (destructuring-bind (vert index)
        (nineveh.mesh.data.primitives:sphere-gpu-arrays)
      (setf *buf-stream*
            (make-buffer-stream vert :index-array index))))

  (unless *cube-stream*
    (destructuring-bind (vert index)
        (nineveh.mesh.data.primitives:cube-gpu-arrays)
      (setf *buf-stream*
            (make-buffer-stream vert :index-array index))))

  (setf *gpu-arr* (first (first (buffer-stream-gpu-arrays *buf-stream*))))

  (unless *albedo-sampler*
    (setf *albedo-sampler*
          (sample
           (dirt:load-image-to-texture
            (asdf:system-relative-pathname
             :render-play "resources/container2.png"))))))

(unless *specular-sampler*
  (setf *specular-sampler*
        (sample
         (dirt:load-image-to-texture
          (asdf:system-relative-pathname
           :render-play "resources/container2_specular.png")))))

(def-simple-main-loop play (:on-start #'init)
  (draw))
